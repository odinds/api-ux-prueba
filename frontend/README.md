This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### Execute spring boot

mvnw spring-boot:run

### Execute spring boot test

mvnw mvn test

### swagger

http://localhost:8080/swagger-ui/



const BASEURL = "http://localhost:8080/tarea/api";


export const apiGetTareas = () => {
    const url = BASEURL + "/getAll" ;

    return fetch(url)
        .then(response => response.json());
};

export const apiAddTarea = (tarea) => {

    const url = BASEURL + "/create";

    const request = {
        method: 'POST',
        body: JSON.stringify(tarea),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    };

    return fetch(url, request)
        .then(response => response.json());

};

export const apiUpdateTarea = (tarea) => {

    const url = BASEURL + "/update";

    const request = {
        method: 'PUT',
        body: JSON.stringify(tarea),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    };

    return fetch(url, request)
        .then(response => response.json())
};

export const apiDeleteTarea = (id) => {

    const url = BASEURL + "/delete/" + id;

    const request = { method: 'DELETE' };

    return fetch(url, request)
        .then(response => response.json())
};
import { createStore, applyMiddleware, combineReducers } from 'redux';

import thunk from 'redux-thunk';
import tareasReducers from './tareasReducers';
import modalReducers from './modalReducer';
import { reducer as formReducer } from 'redux-form'


const mainReducer = combineReducers({
    tarea: tareasReducers,
    form: formReducer,
    modal: modalReducers
});

export default createStore(
    mainReducer,
    applyMiddleware(thunk)
);
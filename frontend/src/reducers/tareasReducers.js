import {
    apiGetTareas,
    apiAddTarea,
    apiUpdateTarea,
    apiDeleteTarea
} from '../lib/api';

const initialState = {
    tareas: []
};

const GET_TAREAS = 'GET_TAREAS';
const ADD_TAREA = 'ADD_TAREA';
const UPDATE_TAREA = 'UPDATE_TAREA';
const DELETE_TAREA = 'DELETE_TAREA';

//actions
const getTareas = (tareas) => ({ type: GET_TAREAS, payload: tareas });
const addTarea = (tareas) => ({ type: ADD_TAREA, payload: tareas });
const updateTarea = (tareas) => ({ type: UPDATE_TAREA, payload: tareas });
const deleteTarea = (id) => ({ type: DELETE_TAREA, payload: id });


export const fetchGetTareas = () => {
    return (dispatch) => {
        apiGetTareas()
            .then(res => {
                console.log("res:"+res);
                dispatch(getTareas(res));
            })
            .catch(res => {
                console.log(res);
            })
    }
};

export const fetchAddTarea = (tareas) => {
    return (dispatch) => {
        apiAddTarea(tareas)
            .then(res => {
                dispatch(addTarea(res));
            })
            .catch(res => {
                console.log(res)
            })
    }
};

export const fetchUpdateTarea = (tarea) => {
    return (dispatch) => {
        apiUpdateTarea(tarea)
            .then(res => {
                dispatch(updateTarea(res));
            })
            .catch(res => {
                console.log(res);
            })
    }
};

export const fetchDeleteTarea = (id) => {
    return (dispatch) => {
        apiDeleteTarea(id)
            .then(res => {
                dispatch(deleteTarea(id));
            })
            .catch(res => {
                console.log(res);
            })
    }
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        //en todos los casos regresamos un objeto nuevo en el cual incluimos todos las propiedades del objeto state con ...state
        case GET_TAREAS:
            //cambiamos el valor de la propiedad post  
            return { ...state, tareas: action.payload };

        case ADD_TAREA:
            console.log(ADD_TAREA, action);
            return {
                ...state,
                tareas: [action.payload, ...state.tareas]
            };


        case UPDATE_TAREA:
            return {
                ...state,
                tareas: [
                    ...state.tareas.map(
                        (tarea) => tarea.identificador === action.payload.identificador ?
                            action.payload
                            : tarea
                    )
                ]
            }

        case DELETE_TAREA:

            return {
                ...state,
                tareas: [...state.tareas.filter(task => task.identificador !== action.payload)]
            }


        default:
            return { ...state };
    }
}


export default reducer;
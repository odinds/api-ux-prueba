import ActionTypes from '../utils/ActionTypes'

const initialState = {
  modalType: null,
  modalProps: {
    open: false
  }
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.SHOW_MODAL:
      return {
        modalProps: action.modalProps,
        modalType: action.modalType,
        type: action.type
      }
    case ActionTypes.HIDE_MODAL:
      return initialState
    default:
      return state
  }
}

export default reducer;
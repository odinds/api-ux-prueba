import React,{ Component } from 'react';
import { Table } from 'react-bootstrap'

class FormTarea extends Component {	 
    constructor(props){
        super(props);
        this.state = {
            editing: true,
            descripcion: props.descripcion, 
            vigente: props.vigente, 
            identificador: props.identificador,
            fechaCreacion: props.fechaCreacion
        }
    }

    hanldeChange(event) {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({ [`${name}`]: value });
    }

    handleSave() {
        if(!this.state.fechaCreacion){
            this.state.fechaCreacion = new Date()
        }
        this.props.funcitonUpdate(
            { 
                descripcion: this.state.descripcion, 
                vigente: this.state.vigente, 
                identificador: this.state.identificador,
                fechaCreacion: this.state.fechaCreacion
            }
        )
        this.props.setState({ opened: false })

        this.setState({ editing: false })
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({ [`${name}`]: value });
    }

    static getDerivedStateFromProps(props, state) {
        if (props.currentRow !== state.lastRow) {
            this.setState({ 
                descripcion: props.descripcion || "", 
                vigente: props.vigente, 
                identificador: props.identificador,
                fechaCreacion: props.fechaCreacion,
                editing: props.opened,
            });
            
        }

        return null;
    }    

	render() {
		return (
            this.state.editing ? 
                <div className="definirTareas">
                <h2>Modificar tarea</h2>
                    <div className="form-group">
                        <Table>
                            <tbody>
                                <tr>
                                    <td>
                                        <label>Identificador</label>
                                    </td>
                                    <td>
                                        <label>{this.state.identificador}</label>
                                    </td>                        
                                    
                                </tr>
                                <tr>
                                    <td>
                                        <label>Descrtipción</label>
                                    </td>
                                    <td>
                                        <input name="descripcion" required value={this.state.descripcion} onChange={this.hanldeChange.bind(this)}/>
                                    </td>                        
                                    
                                </tr>
                                <tr>
                                    <td>
                                        <label>vigente</label>
                                    </td>
                                    <td>
                                        <input type="checkbox" id="cbox2" name="vigente" checked={this.state.vigente} onChange={this.handleInputChange.bind(this)}/>
                                    </td>                        
                                </tr>   
                                <tr>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        <button onClick={this.handleSave.bind(this)}>Guardar</button>
                                    </td>                        
                                    
                                </tr>                                            
                            </tbody>
                        </Table>
                    </div>


                </div>
            :            
			<div className="definirTareas">
                <h2>Crear tarea</h2>
                <Table>
                    <tbody>
                        <tr>
                            <td>
                                <label>Identificador</label>
                            </td>
                            <td>
                                <label>{this.state.identificador}</label>
                            </td>                        
                            
                        </tr>
                        <tr>
                            <td>
                                <label>Descrtipción</label>
                            </td>
                            <td>
                                <input name="descripcion" value={this.state.descripcion} onChange={this.hanldeChange.bind(this)}/>
                            </td>                        
                            
                        </tr>
                        <tr>
                            <td>
                                <label>vigente</label>
                            </td>
                            <td>
                                <input type="checkbox" id="cbox2" name="vigente" checked={this.state.vigente} onChange={this.handleInputChange.bind(this)}/>
                            </td>                        
                        </tr>   
                        <tr>
                            <td>
                                
                            </td>
                            <td>
                                <button onClick={this.handleSave.bind(this)}>Guardar</button>
                            </td>                        
                            
                        </tr>                                            
                    </tbody>
                </Table>
                
            </div>
		);
	}
}

export default FormTarea;
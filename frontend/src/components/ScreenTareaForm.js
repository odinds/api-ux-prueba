import React,{ Component } from 'react';
import TareaForm from './TareaForm' ;

class ScreenTareaForm extends Component {	 
    constructor(props){
        super(props);
        this.state = {
            editing: true,
            descripcion: props.descripcion, 
            vigente: props.vigente, 
            identificador: props.identificador,
            fechaCreacion: props.fechaCreacion
        }
    }

    hanldeChange(event) {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({ [`${name}`]: value });
    }

    handleSave() {

    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({ [`${name}`]: value });
    }

    static getDerivedStateFromProps(props, state) {
        return null;
    }

    handleSubmit = (values) => {

      if(this.props.dataFromParent.isUpdate){
        this.props.funcitonUpdate(
          { 
              descripcion: values.descripcion, 
              vigente: values.vigente?values.vigente:false, 
              identificador: this.props.dataFromParent.tarea.identificador,
              fechaCreacion: this.props.dataFromParent.tarea.fechaCreacion
          }
      )
      }else{
        this.props.funcitonCreate(
          { 
            descripcion: values.descripcion, 
            vigente: values.vigente?values.vigente:false, 
            fechaCreacion: new Date()
          }
                  
        )
      }
      this.props.funcitonGet();
      this.props.setState({ opened: false })
    }

	render() {
		return (
        this.props.dataFromParent.isUpdate?( 
                <div className="definirTareas">
                  <h2>Modificar tarea</h2>
                  
                      <TareaForm onSubmit={this.handleSubmit} dataFromParent={this.props.dataFromParent.tarea}/>
                  
                </div>
              )
            :(
              <div className="definirTareas">
                <h2>Crear tarea</h2>
                
                  <TareaForm onSubmit={this.handleSubmit}/>
                
              </div>
            )
		);
	}
}

export default ScreenTareaForm;
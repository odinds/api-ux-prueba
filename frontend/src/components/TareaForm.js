import React from 'react'
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form'

const validate = values => {
  const errors = {}
  if (!values.descripcion) {
    errors.descripcion = 'Required'
  } else if (values.descripcion.length > 100) {
    errors.descripcion = 'Must be 15 characters or less'
  }

  return errors
}

const warn = values => {
  const warnings = {}
  if (values.age < 19) {
    warnings.age = 'Hmm, you seem a bit young...'
  }
  return warnings
}

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
  <div>
    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} type={type}/>
      {touched &&
        ((error && <span className="fieldError">{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
)

let stateForm = {

};

let TareaForm = props => {
  const { handleSubmit, pristine, reset, submitting, dataFromParent } = props

  stateForm = {  
    tarea:{
        descripcion: '',
        vigente: false
    }
  };

  if(dataFromParent){
    stateForm = {  
        tarea:{
            descripcion: dataFromParent.descripcion,
            vigente: dataFromParent.vigente
        }

    };
  }

  return (
    dataFromParent?(
    <form onSubmit={handleSubmit}>
      <Field name="descripcion" type="textarea" component={renderField} label="Descripción" />
      <br/>
      <Field name="vigente" type="checkbox" component={renderField} label="Vigente"/>
      <br/>
      <div>
        <button type="submit" disabled={submitting}>
          Modificar Tarea
        </button>
        <button type="button" disabled={pristine || submitting} onClick={reset}>
           Borrar Datos
        </button>
      </div>
    </form>)
    :(
    <form onSubmit={handleSubmit}>
      <Field name="descripcion" type="textarea" component={renderField} label="Descripción" />
      <br/>
      <Field name="vigente" type="checkbox" component={renderField} label="Vigente"/>
      <br/>
      <div>
        <button type="submit" disabled={submitting}>
        Crear Tarea
        </button>
        <button type="button" disabled={pristine || submitting} onClick={reset}>
           Borrar Datos
        </button>
      </div>
    </form>)    
  )
}


const mapStateToProps = state => ({
    initialValues: stateForm.tarea
  });


TareaForm = reduxForm({
  form: 'tareaForm', // a unique identifier for this form
  validate, // <--- validation function given to redux-form
  warn // <--- warning function given to redux-form
})(TareaForm);

TareaForm = connect(mapStateToProps)(TareaForm);

export default TareaForm;
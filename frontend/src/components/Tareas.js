import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchGetTareas, fetchAddTarea, fetchUpdateTarea, fetchDeleteTarea} from '../reducers/tareasReducers'
import { Table } from 'react-bootstrap'
import ScreenTareaForm from './ScreenTareaForm'

class Tareas extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            body: '',
            opened: false,
            tarea: null,
        }
        this.displayForm = this.displayForm.bind(this);
    }    

    componentDidMount() {
        this.props.fetchGetTareas();
    }
    
    displayForm = (tarea,upateT) => {
		//const { opened } = this.state;
		this.setState({
			opened: !this.state.opened,
            data:{
                tarea: tarea,
                isUpdate: upateT
            }
		});
    }

    render() {
        const tareas = this.props.tareas;
        const { opened,data} = this.state;

        return (
            <section>
                <button onClick={() => this.displayForm(null,false)}> Crear Tarea </button>
                <br/>
               <br/>
            <div className="tareas">
                <Table responsive>
                    <thead>
                        <tr>
                            <th>Identificador</th>
                            <th>Fecha Creación</th>
                            <th>Descrtipción</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            tareas.map(j => (
                                <tr>
                                    <td>{j.identificador}</td>
                                    <td>{j.fechaCreacion}</td>
                                    <td>{j.descripcion}</td>
                                    
                                    <td>
                                        <button onClick={() => this.displayForm(j,true)}> Actualizar </button>
                                        <button onClick={() => this.props.fetchDeleteTarea(j.identificador)}> Eliminar </button>
                                    </td>
                                </tr>    
                            ))
                        }
                    </tbody>
                </Table>
            </div>
            <br/>
            <br/>

            {(opened && data.isUpdate) && (					
					<ScreenTareaForm dataFromParent={data} setState={state => this.setState(state)} funcitonUpdate = {this.props.fetchUpdateTarea} funcitonGet = {this.props.fetchGetTareas}/>
            )}

            {(opened && !data.isUpdate) && (					
					<ScreenTareaForm dataFromParent={data} setState={state => this.setState(state)} funcitonCreate = {this.props.fetchAddTarea} funcitonGet = {this.props.fetchGetTareas}/>
            )}            

        </section>
        )
    }
 
}

export default connect(    (state) => ({
    tareas: state.tarea.tareas,
}), {
     fetchGetTareas, fetchAddTarea, fetchUpdateTarea, fetchDeleteTarea
} )(Tareas)
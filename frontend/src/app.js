import React from 'react';
import Tareas from './components/Tareas';

const App = () => (
    <main>
      <h1>Tareas</h1>
      <Tareas>
        
      </Tareas>
    </main>
)

export default App;
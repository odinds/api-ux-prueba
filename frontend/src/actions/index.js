import axios from 'axios'

export const SHOW_TAREAS = 'SHOW_TAREAS'
export const UPDATE_TAREA = 'UPDATE_TAREA';
export const QUITAR_TAREA = 'QUITAR_TAREA';

export function showTareas() {
    
    return (dispatch, getState) => {
        console.log("response");
        axios.get('http://localhost:8080/tarea/api/getAll')
            .then((response) => {
                dispatch( { type: SHOW_TAREAS, payload: response.data } ) 
            }) 
    }
    
} 
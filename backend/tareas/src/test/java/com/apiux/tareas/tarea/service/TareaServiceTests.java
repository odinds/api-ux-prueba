package com.apiux.tareas.tarea.service;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.apiux.tareas.exceptions.TareaApplicationExeption;
import com.apiux.tareas.tarea.dtos.TareaDto;
import com.apiux.tareas.tarea.model.Tarea;
import com.apiux.tareas.tarea.model.repository.TareaRepository;
import com.apiux.tareas.tarea.services.impl.TareaServiceImpl;
import com.apiux.tareas.utils.GenericMapper;

@RunWith(MockitoJUnitRunner.class)
public class TareaServiceTests {

    @Mock
    private TareaRepository repository; 

    @Mock
	private GenericMapper genericMapper;
    
    @Mock
	private Validator validator;    
    
    @InjectMocks
    private TareaServiceImpl tareaService;    

    @Before
    public void setUp() throws Exception {
    }
    
    @Test
    public void whenSaveTarea_thenCreateTarea() throws Exception {
        
        Tarea tarea = new Tarea();
        tarea.setDescripcion("desctipcion");
        tarea.setFechaCreacion(LocalDateTime.now());
        tarea.setVigente(Boolean.TRUE);
        
        Tarea tareaCreate = new Tarea();
        tareaCreate.setIdentificador(1L);
        tareaCreate.setDescripcion("desctipcion");
        tareaCreate.setFechaCreacion(LocalDateTime.now());
        tareaCreate.setVigente(Boolean.TRUE);
        
        TareaDto tareadto = new TareaDto();
        tareadto.setDescripcion("desctipcion");
        tareadto.setFechaCreacion(LocalDateTime.now());
        tareadto.setVigente(Boolean.TRUE);
        
        TareaDto tareadtoResponse = new TareaDto();
        tareadtoResponse.setIdentificador(1L);
        tareadtoResponse.setDescripcion("desctipcion");
        tareadtoResponse.setFechaCreacion(LocalDateTime.now());
        tareadtoResponse.setVigente(Boolean.TRUE);        
        
        Mockito.when(genericMapper.dtoToEntity(new Tarea(), tareadto)).thenReturn(tarea);
        Mockito.when(repository.save(tarea)).thenReturn(tareaCreate);
        Mockito.when(genericMapper.entityToDto(tareaCreate, new TareaDto())).thenReturn(tareadtoResponse);

        //Assert.assertEquals(tareaService.save(tareadto).getIdentificador(),tareadtoResponse.getIdentificador());
    }  
    
    @Test
    public void whenSaveTarea_thenCreateTarea_ApplicationExepction() throws Exception {
    	String assertMessage = "descripcion: descripcion is mandatory";
    	
        TareaDto tareadto = new TareaDto();
        tareadto.setDescripcion("");
        tareadto.setFechaCreacion(LocalDateTime.now());
        tareadto.setVigente(Boolean.TRUE);
     
        Exception exception = assertThrows(TareaApplicationExeption.class, () -> {
        	tareaService.save(tareadto);
        });        
        
        Assert.assertEquals(assertMessage, exception.getMessage());
    }    
    
    @Test
    public void whenUpdateTarea_thenUpdateTarea() throws Exception {
    	Tarea tarea = new Tarea();
    	tarea.setIdentificador(1L);
        tarea.setDescripcion("desctipcion");
        tarea.setFechaCreacion(LocalDateTime.now());
        tarea.setVigente(Boolean.TRUE);
        
        Tarea tareaCreate = new Tarea();
        tareaCreate.setIdentificador(1L);
        tareaCreate.setDescripcion("desctipcion 123");
        tareaCreate.setFechaCreacion(LocalDateTime.now());
        tareaCreate.setVigente(Boolean.TRUE);
        
        TareaDto tareadto = new TareaDto();
        tareadto.setIdentificador(1L);
        tareadto.setDescripcion("desctipcion");
        tareadto.setFechaCreacion(LocalDateTime.now());
        tareadto.setVigente(Boolean.TRUE);
        
        TareaDto tareadtoResponse = new TareaDto();
        tareadtoResponse.setIdentificador(1L);
        tareadtoResponse.setDescripcion("desctipcion 123");
        tareadtoResponse.setFechaCreacion(LocalDateTime.now());
        tareadtoResponse.setVigente(Boolean.TRUE);        
        
        Mockito.when((Tarea)genericMapper.dtoToEntity(new Tarea(), tareadto)).thenReturn(tarea);
        Mockito.when(repository.save(tarea)).thenReturn(tareaCreate);
        //Mockito.when((TareaDto)genericMapper.entityToDto(tareaCreate, new TareaDto() )).thenReturn(tareadtoResponse);

        //Assert.assertEquals(tareaService.update(tareadto).getIdentificador(),tareadtoResponse.getIdentificador());
    }    
    
    @Test
    public void whenUpdateTarea_thenUpdateTarea_ApplicationExepction() throws Exception {
    	String assertMessage = "vigente: vigente is mandatory";
    	
        TareaDto tareadto = new TareaDto();
        tareadto.setDescripcion("Des");
        tareadto.setFechaCreacion(LocalDateTime.now());
        tareadto.setVigente(null);
     
        Exception exception = assertThrows(TareaApplicationExeption.class, () -> {
        	tareaService.update(tareadto);
        });        
        
        Assert.assertEquals(assertMessage, exception.getMessage());
    }      
    
    @Test
    public void whenDeleteTarea_thenDeleteTarea() throws Exception {
        Long idTarea = 1L;

        Mockito.doNothing().when(repository).deleteById(idTarea);
        Assert.assertTrue(tareaService.delete(idTarea));
    }  
    
    
    @Test
    public void whenGetTareas_thenGetsTarea() throws Exception {
        List<Tarea> listEntities = new ArrayList<>();
        List<TareaDto> listDtos = new ArrayList<>();
    	Tarea tarea = new Tarea();
    	tarea.setIdentificador(1L);
        tarea.setDescripcion("desctipcion2");
        tarea.setFechaCreacion(LocalDateTime.now());
        tarea.setVigente(Boolean.TRUE);
        
    	Tarea tarea2 = new Tarea();
    	tarea.setIdentificador(2L);
        tarea2.setDescripcion("desctipcion3");
        tarea2.setFechaCreacion(LocalDateTime.now());
        tarea2.setVigente(Boolean.TRUE);   
        
        listEntities.add(tarea);
        listEntities.add(tarea2);

        TareaDto tareadto = new TareaDto();
        tareadto.setIdentificador(1L);
        tareadto.setDescripcion("desctipcion2");
        tareadto.setFechaCreacion(LocalDateTime.now());
        tareadto.setVigente(Boolean.TRUE);
        
        TareaDto tareadto2 = new TareaDto();
        tareadto2.setIdentificador(2L);
        tareadto2.setDescripcion("desctipcion3");
        tareadto2.setFechaCreacion(LocalDateTime.now());
        tareadto2.setVigente(Boolean.TRUE);   
        
        Mockito.when(genericMapper.entityToDto(tarea, new TareaDto() )).thenReturn(tareadto);        
        Mockito.when(genericMapper.entityToDto(tarea2, new TareaDto() )).thenReturn(tareadto2);        
        
        
        listDtos.add(tareadto);
        listDtos.add(tareadto2);
        
		Mockito.when(repository.findAll()).thenReturn(listEntities);
        Assert.assertEquals(tareaService.getAll().size(), 2);
    }     
}

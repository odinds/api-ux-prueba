package com.apiux.tareas.tarea.infraestructure;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.apiux.tareas.exceptions.TareaApplicationExeption;
import com.apiux.tareas.exceptions.TechnicalExeption;
import com.apiux.tareas.tarea.dtos.TareaDto;
import com.apiux.tareas.tarea.services.TareaService;

@RunWith(MockitoJUnitRunner.class)
public class TareaRestControllerTests {


    @Mock
    private TareaService service; 
    
    @InjectMocks
    private TareaRestController tareaRestController;    

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void whenPostTarea_thenCreateTarea() throws Exception {
        TareaDto tarea = new TareaDto();
        tarea.setDescripcion("desctipcion");
        tarea.setFechaCreacion(LocalDateTime.now());
        tarea.setVigente(Boolean.TRUE);
        Mockito.when(service.save(Mockito.any())).thenReturn(tarea);

        Assert.assertEquals(tareaRestController.createTarea(tarea).getStatusCode(),HttpStatus.CREATED) ;
    }
    
    @Test
    public void whenPostTarea_thenCreateTarea_ApplicationException() throws Exception {
    	TareaDto tarea = new TareaDto();
    	tarea.setDescripcion(null);
    	tarea.setFechaCreacion(LocalDateTime.now());
    	tarea.setVigente(Boolean.TRUE);
    	Mockito.when(service.save(Mockito.any())).thenThrow(TareaApplicationExeption.class);
    	
    	Assert.assertEquals(tareaRestController.createTarea(tarea).getStatusCode(),HttpStatus.BAD_REQUEST);
    }
    
    @Test
    public void whenPostTarea_thenCreateTarea_TechnicalException() throws Exception {
    	TareaDto tarea = new TareaDto();
    	tarea.setDescripcion(null);
    	tarea.setFechaCreacion(LocalDateTime.now());
    	tarea.setVigente(Boolean.TRUE);
    	Mockito.when(service.save(Mockito.any())).thenThrow(TechnicalExeption.class);
    	
    	Assert.assertEquals(tareaRestController.createTarea(tarea).getStatusCode(),HttpStatus.INTERNAL_SERVER_ERROR) ;
    }
    
    @Test
    public void whenPutTarea_thenUpdateTarea() throws Exception {
        TareaDto tarea = new TareaDto();
        tarea.setIdentificador(1L);
        tarea.setDescripcion("desctipcion");
        tarea.setFechaCreacion(LocalDateTime.now());
        tarea.setVigente(Boolean.TRUE);
        Mockito.when(service.update(Mockito.any())).thenReturn(tarea);

        Assert.assertEquals(tareaRestController.updateTarea(tarea).getStatusCode(),HttpStatus.CREATED) ;
    }    
    
    @Test
    public void whenPutTarea_thenUpdateTarea_ApplicationException() throws Exception {
    	TareaDto tarea = new TareaDto();
    	tarea.setIdentificador(1L);
    	tarea.setDescripcion("desctipcion");
    	tarea.setFechaCreacion(LocalDateTime.now());
    	tarea.setVigente(Boolean.TRUE);
    	Mockito.when(service.update(Mockito.any())).thenThrow(TareaApplicationExeption.class);
    	
    	Assert.assertEquals(tareaRestController.updateTarea(tarea).getStatusCode(),HttpStatus.BAD_REQUEST);
    }    
    
    @Test
    public void whenPutTarea_thenUpdateTarea_TechnicalException() throws Exception {
    	TareaDto tarea = new TareaDto();
    	tarea.setIdentificador(1L);
    	tarea.setDescripcion("desctipcion");
    	tarea.setFechaCreacion(LocalDateTime.now());
    	tarea.setVigente(Boolean.TRUE);
    	Mockito.when(service.update(Mockito.any())).thenThrow(TechnicalExeption.class);
    	
    	Assert.assertEquals(tareaRestController.updateTarea(tarea).getStatusCode(),HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @Test
    public void whenDeleteTarea_thenDeleteTarea() throws Exception {
        TareaDto tarea = new TareaDto();
        tarea.setIdentificador(1L);

        Mockito.when(service.delete(Mockito.any())).thenReturn(Boolean.TRUE);

        Assert.assertEquals(tareaRestController.deleteTarea(tarea.getIdentificador()).getStatusCode(),HttpStatus.OK) ;
    } 
    
    @Test
    public void whenDeleteTarea_thenDeleteTarea_ApplicationException() throws Exception {
    	TareaDto tarea = new TareaDto();
    	tarea.setIdentificador(1L);
    	
    	Mockito.when(service.delete(Mockito.any())).thenThrow(TareaApplicationExeption.class);
    	
    	Assert.assertEquals(tareaRestController.deleteTarea(tarea.getIdentificador()).getStatusCode(),HttpStatus.BAD_REQUEST) ;
    } 
    
    @Test
    public void whenDeleteTarea_thenDeleteTarea_TechnicalException() throws Exception {
    	TareaDto tarea = new TareaDto();
    	tarea.setIdentificador(1L);
    	
    	Mockito.when(service.delete(Mockito.any())).thenThrow(TechnicalExeption.class);
    	
    	Assert.assertEquals(tareaRestController.deleteTarea(tarea.getIdentificador()).getStatusCode(),HttpStatus.INTERNAL_SERVER_ERROR) ;
    } 
    
    
    @Test
    public void whenGetTarea_thenGetTareas() throws Exception {
    	List<TareaDto> data = new ArrayList<>();
    	TareaDto tarea = new TareaDto();
    	tarea.setIdentificador(1L);
    	tarea.setDescripcion("desctipcion");
    	tarea.setFechaCreacion(LocalDateTime.now());
    	tarea.setVigente(Boolean.TRUE);

    	TareaDto tarea2 = new TareaDto();
    	tarea2.setIdentificador(2L);
    	tarea2.setDescripcion("desctipcion 2");
    	tarea2.setFechaCreacion(LocalDateTime.now());
    	tarea2.setVigente(Boolean.TRUE);
    	
    	data.add(tarea);
    	data.add(tarea2);

        Mockito.when(service.getAll()).thenReturn(data);

        ResponseEntity<List<TareaDto>> response= tareaRestController.getTarea();
        
        Assert.assertEquals(response.getStatusCode(),HttpStatus.OK) ;
        Assert.assertEquals(response.getBody() ,data) ;
    }     
    
    @Test
    public void whenGetTarea_thenGetTareas_TechnicalException() throws Exception {
    	List<TareaDto> data = new ArrayList<>();
    	TareaDto tarea = new TareaDto();
    	tarea.setIdentificador(1L);
    	tarea.setDescripcion("desctipcion");
    	tarea.setFechaCreacion(LocalDateTime.now());
    	tarea.setVigente(Boolean.TRUE);
    	
    	TareaDto tarea2 = new TareaDto();
    	tarea2.setIdentificador(2L);
    	tarea2.setDescripcion("desctipcion 2");
    	tarea2.setFechaCreacion(LocalDateTime.now());
    	tarea2.setVigente(Boolean.TRUE);
    	
    	data.add(tarea);
    	data.add(tarea2);
    	
    	Mockito.when(service.getAll()).thenThrow(TechnicalExeption.class);
    	
    	ResponseEntity<List<TareaDto>> response= tareaRestController.getTarea();
    	
    	Assert.assertEquals(response.getStatusCode(),HttpStatus.INTERNAL_SERVER_ERROR) ;
    }     
    

}

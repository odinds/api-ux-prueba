package com.apiux.tareas.tarea.services;

import java.util.List;

import javax.validation.Valid;

import com.apiux.tareas.exceptions.TareaApplicationExeption;
import com.apiux.tareas.exceptions.TechnicalExeption;
import com.apiux.tareas.tarea.dtos.TareaDto;

/**
 * 
 * @author daniel.sarmiento
 *
 */
public interface TareaService {

	/**
	 * 
	 * @param tarea
	 * @return
	 * @throws TareaApplicationExeption
	 * @throws TechnicalExeption
	 */
	TareaDto save(TareaDto tareaDto) throws TareaApplicationExeption,TechnicalExeption;

	/**
	 * 
	 * @param tareaDto
	 * @return
	 * @throws TareaApplicationExeption
	 * @throws TechnicalExeption
	 */
	TareaDto update(TareaDto tareaDto) throws TareaApplicationExeption,TechnicalExeption;

	/**
	 * 
	 * @param id
	 * @return
	 * @throws TareaApplicationExeption
	 * @throws TechnicalExeption
	 */
	Boolean delete(Long id) throws TareaApplicationExeption,TechnicalExeption;

	/**
	 * 
	 * @return
	 * @throws TechnicalExeption
	 */
	List<TareaDto> getAll() throws TechnicalExeption;;

}

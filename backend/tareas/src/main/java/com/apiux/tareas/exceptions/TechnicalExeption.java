package com.apiux.tareas.exceptions;

/**
 * 
 * @author daniel.sarmiento
 *
 */
public class TechnicalExeption extends Exception {

	public TechnicalExeption(String message) {
		super(message);
	}
}

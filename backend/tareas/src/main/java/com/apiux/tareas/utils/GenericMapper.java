package com.apiux.tareas.utils;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.apiux.tareas.tarea.dtos.DTOEntity;

@Component
public class GenericMapper{
	
    @Autowired
    private ModelMapper modelMapper;
    
    public DTOEntity  entityToDto(Object obj, DTOEntity mapper) {
    	return modelMapper.map(obj, mapper.getClass());
    }
    
    public Object  dtoToEntity(Object obj, DTOEntity mapper) {
    	return modelMapper.map(mapper, obj.getClass());
    }
}

package com.apiux.tareas.exceptions;


public class TareaApplicationExeption extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4155209654477135510L;

	public TareaApplicationExeption(String message) {
		super(message);
	}
}

package com.apiux.tareas.tarea.model.repository;

import org.springframework.data.repository.CrudRepository;

import com.apiux.tareas.tarea.model.Tarea;

/**
 * 
 * @author daniel.sarmiento
 *
 */
public interface TareaRepository extends CrudRepository<Tarea, Long>{

}

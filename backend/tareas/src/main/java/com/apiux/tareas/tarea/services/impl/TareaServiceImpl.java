package com.apiux.tareas.tarea.services.impl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.apiux.tareas.exceptions.TareaApplicationExeption;
import com.apiux.tareas.exceptions.TechnicalExeption;
import com.apiux.tareas.tarea.dtos.TareaDto;
import com.apiux.tareas.tarea.model.Tarea;
import com.apiux.tareas.tarea.model.repository.TareaRepository;
import com.apiux.tareas.tarea.services.TareaService;
import com.apiux.tareas.utils.GenericMapper;

@Service
public class TareaServiceImpl implements TareaService {

	@Autowired
	private GenericMapper genericMapper;

	@Autowired
	private TareaRepository tareaRepository;

	public TareaDto save(TareaDto tareaDto) throws TareaApplicationExeption, TechnicalExeption {
		try {
			validateInputWithInjectedValidator(tareaDto);
		} catch (ConstraintViolationException e) {
			throw new TareaApplicationExeption(e.getMessage());
		}
		
		Tarea entity = (Tarea)genericMapper.dtoToEntity(new Tarea(),tareaDto);
		Tarea entityCreate = tareaRepository.save(entity);

		
		TareaDto dto = (TareaDto) genericMapper.entityToDto(entityCreate, new TareaDto());
		return dto;
	}

	@Override
	public TareaDto update(TareaDto tareaDto) throws TareaApplicationExeption, TechnicalExeption {
		try {
			validateInputWithInjectedValidator(tareaDto);
		} catch (ConstraintViolationException e) {
			throw new TareaApplicationExeption(e.getMessage());
		}
		Tarea tarea = (Tarea)genericMapper.dtoToEntity(new Tarea(),tareaDto);
		Tarea tareaCreate = tareaRepository.save(tarea);

		TareaDto dto = (TareaDto) genericMapper.entityToDto(tareaCreate, new TareaDto());
		return dto;
	}

	@Override
	public Boolean delete(Long id) throws TareaApplicationExeption, TechnicalExeption {
		try {
			tareaRepository.deleteById(id);
		}catch(EmptyResultDataAccessException e) {
			throw new TechnicalExeption(e.getMessage());
		}
		return Boolean.TRUE;
	}

	public List<TareaDto> getAll() throws TechnicalExeption {
		List<Tarea> tareas =  
				  StreamSupport.stream(tareaRepository.findAll().spliterator(), false)
				    .collect(Collectors.toList());
		
		List<TareaDto> tareasDto = tareas.stream().map(entity -> (TareaDto) genericMapper.entityToDto(entity, new TareaDto())).collect(Collectors.toList()); 
		return tareasDto;
	}

	void validateInputWithInjectedValidator(TareaDto tareaDto) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<TareaDto>> violations = validator.validate(tareaDto);
		if(!violations.isEmpty()) {
			throw new ConstraintViolationException(violations);
		}
	}

}

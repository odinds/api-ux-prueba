package com.apiux.tareas.tarea.infraestructure;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apiux.tareas.exceptions.TareaApplicationExeption;
import com.apiux.tareas.exceptions.TechnicalExeption;
import com.apiux.tareas.tarea.dtos.TareaDto;
import com.apiux.tareas.tarea.services.TareaService;

@RestController
@RequestMapping("/tarea/api")
public class TareaRestController {

	@Autowired
	private TareaService tareaService;
	
    @PostMapping("/create")
    public ResponseEntity<TareaDto> createTarea(@RequestBody TareaDto tareaDto) {
        HttpStatus status = HttpStatus.CREATED;
		try {
			TareaDto saved = tareaService.save(tareaDto);
			return new ResponseEntity<>(saved, status);
		} catch (TareaApplicationExeption e) {
			status = HttpStatus.BAD_REQUEST;
			return new ResponseEntity<>(null, status);
		} catch (TechnicalExeption e) {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			return new ResponseEntity<>(null, status);
		}
    }

    @PutMapping("/update")
	public ResponseEntity<TareaDto> updateTarea(@RequestBody TareaDto tareaDto) {
        HttpStatus status = HttpStatus.CREATED;
		try {
			TareaDto saved = tareaService.update(tareaDto);
			return new ResponseEntity<>(saved, status);
		} catch (TareaApplicationExeption e) {
			status = HttpStatus.BAD_REQUEST;
			return new ResponseEntity<>(null, status);
		} catch (TechnicalExeption e) {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			return new ResponseEntity<>(null, status);
		}
	}

    @DeleteMapping("/delete/{id}")
	public ResponseEntity<Boolean> deleteTarea(@PathVariable("id") Long id) {
        HttpStatus status = HttpStatus.OK;
		try {
			Boolean deleted = tareaService.delete(id);
			return new ResponseEntity<>(deleted, status);
		} catch (TareaApplicationExeption e) {
			status = HttpStatus.BAD_REQUEST;
			return new ResponseEntity<>(Boolean.FALSE, status);
		} catch (TechnicalExeption e) {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			return new ResponseEntity<>(Boolean.FALSE, status);
		}
	}

    @GetMapping("/getAll")
	public ResponseEntity<List<TareaDto>> getTarea() {
        HttpStatus status = HttpStatus.OK;
		try {
			List<TareaDto> data = tareaService.getAll();
			return new ResponseEntity<>(data, status);
		} catch (TechnicalExeption e) {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			return new ResponseEntity<>(null, status);
		}
	}
       
}

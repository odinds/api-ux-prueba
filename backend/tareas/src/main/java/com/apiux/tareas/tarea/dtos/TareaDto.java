package com.apiux.tareas.tarea.dtos;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author daniel.sarmiento
 *
 */
public class TareaDto implements Serializable, DTOEntity{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6848789981498700375L;
	
	private Long identificador;
	
	@NotBlank(message = "descripcion is mandatory")
	private String descripcion;
	
	private LocalDateTime fechaCreacion;
	
	@NotNull(message = "vigente is mandatory")
	private Boolean vigente;
	
	public Long getIdentificador() {
		return identificador;
	}
	public void setIdentificador(Long identificador) {
		this.identificador = identificador;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public LocalDateTime getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(LocalDateTime fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Boolean getVigente() {
		return vigente;
	}
	public void setVigente(Boolean vigente) {
		this.vigente = vigente;
	}
	
	
}

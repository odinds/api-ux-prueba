# Api Ux Prueba


Se entrega 2 proyectos java en spring boot, y react - redux

En la carpeta backend proyecto java spring. 

En la carpeta frontend proyecto react redux.



#Backend

Java 11
Spring boot 2.4
Maven


#Ejecucion spring boot

JAVA:
java -version

MAVEN:
mvn -v

SPRING BOOT:
mvn spring-boot:run

http://localhost:8080

SPRING NOOT: test
mvn spring-boot:test

SWAGGER:
http://localhost:8080/swagger-ui/


#Frontend

PRE: install nodejs and npm


DEPENDENCES:
npm install

EXECUTE:
npm start 

http://localhost:3000/

